//
//  nullstream.h
//  hfsinspect
//
//  Created by Adam Knight on 5/30/13.
//  Copyright (c) 2013 Adam Knight. All rights reserved.
//

#ifndef hfsinspect_nullstream_h
#define hfsinspect_nullstream_h

#include <stdio.h>

FILE* nullstream();

#endif
